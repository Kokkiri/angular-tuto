// Angular 12.x​​​​​​‌​​‌‌‌‌​​‌​‌​​​‌‌​‌‌​​​​‌ code
import { Component, NgModule } from '@angular/core';


@Component({
    selector: 'counter-component',
    template: `
     <input id="intervalInput"/>
     <button id="intervalSetButton">Set interval</button>
  `
})
export class CounterComponent {
}

// #region Preview
@Component({
    selector: 'display-component',
    template: `
    <counter-component [message]="'Hello world'" (tick)=counterTick($event)></counter-component>
    <div>{{message}} {{counter}}</div>
  `
})
export class DisplayComponent {
    public counter: number = 0;
    public message: string;

    public counterTick(message: string): void {
        this.message = message;
        this.counter++;
    }
}

@Component({
    template: `<display-component></display-component>`
})
export class PreviewComponent { }
// #endregion Preview

// #region Module declaration - Do not Change
@NgModule({
    declarations: [PreviewComponent, DisplayComponent, CounterComponent],
    entryComponents: [PreviewComponent]
})
export class PreviewModule { }
// #endregion Module declaration

