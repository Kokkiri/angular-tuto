import { Component, OnInit } from '@angular/core';
import { AbstractControl, Form, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  get name() {
    return this.form.get('name')
  }

  public form: FormGroup = new FormGroup(
    {
      name: new FormControl('', this.validatorPaul),
      login: new FormGroup({
        email: new FormControl(''),
        password: new FormControl('', this.passwordMatch),
      }),
    },
//     { validators: this.passwordMatch }
  )

  validatorPaul(abstractControl: AbstractControl): { [s: string]: boolean } | null {
    if (abstractControl.value === 'paul') {
      return { notPaul: true }
    } else {
      return null
    }
  }

  ngOnInit(): void {
//     console.log('validator', this.form);
  }

//   public form: FormGroup = new FormGroup(
//     {
//       name: new FormControl(""),
//       email: new FormControl("", Validators.email),
//       password: new FormControl(""),
//       confirmation: new FormControl("")
//     },
//     { validators: this.passwordMatch() }
//   );

  passwordMatch(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      return control.get('login')!.get('password')!.value != control.get('confirmation')!.value
        ? { noMatch: true }
        : null;
    };
  }

  public submit(): void {
    console.log('validator', this.form.valid);
    console.log('errors', this.form.get('password')?.errors);
  }
}
