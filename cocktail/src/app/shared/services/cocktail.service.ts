import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, filter, Observable, tap, map } from 'rxjs';
import { Cocktail } from '../interface/cocktail.interface';

@Injectable({
  providedIn: 'root'
})
export class CocktailService {
  public cocktails$: BehaviorSubject<Cocktail[] | []> = new BehaviorSubject< Cocktail[] | [] >([]);

//   public getCocktail(index: number) {
//     return this.cocktails$.value[index];
//   }

  public getCocktail(index: number): Observable<Cocktail> {
    return this.cocktails$.pipe(
      filter((cocktails: Cocktail[]) => cocktails !== null),
      map((cocktails: Cocktail[]) => cocktails[index])
    )
  }

  public addCocktail(cocktail: Cocktail): Observable<Cocktail> {
    return this.http.post<Cocktail>('https://restapi/api/cocktails', cocktail).pipe(
    tap((savedCocktail: Cocktail) => {
      const value = this.cocktails$.value;
      this.cocktails$.next([...value, savedCocktail]);
    }))
  }

  public editCocktail(cocktailsId: string, editedCocktail: Cocktail): Observable<Cocktail> {
    return this.http.patch<Cocktail>(
      `https://restapi/api/cocktails/${cocktailsId}`,
      editedCocktail
    ).pipe(
      tap((savedCocktail: Cocktail) => {
        const value = this.cocktails$.value;
        this.cocktails$.next(
          value.map((cocktail: Cocktail) => {
            if (cocktail.name === savedCocktail.name) {
              return savedCocktail;
            } else {
              return cocktail;
            }
          })
        );
      })
    )
  }

  public fetchCocktails(): Observable<Cocktail[]> {
    return this.http.get<Cocktail[] | []>('https://restapi.fr/api/cocktails').pipe(
      tap((cocktails: Cocktail[]) => {
        this.cocktails$.next(cocktails);
      })
    )
  }

  constructor(private http: HttpClient) {
//     this.seed()
  }

  public seed() {
    this.http.post('https://restapi.fr/api/cocktails', {
      name: 'Blender',
      img: 'assets/img/blender_logo.svg',
      description: 'Logiciel de modélisation 3D',
      ingredients: [
        {
          name: 'stylet',
          quantity: 1
        },
        {
          name: 'tablette',
          quantity: 1
        }
      ]
    },).subscribe();

    this.http.post('https://restapi.fr/api/cocktails', {
      name: 'Godot',
      img: 'assets/img/godot_logo.png',
      description: 'Moteur de jeux-vidéo',
      ingredients: [
        {
          name: 'creativity',
          quantity: 10
        }
      ]
    },).subscribe();

    this.http.post('https://restapi.fr/api/cocktails', {
      name: 'Krita',
      img: 'assets/img/krita_logo.png',
      description: 'Logiciel de dessin et dessin d\'animation',
      ingredients: [
        {
          name: 'crayon',
          quantity: 2
        },
        {
          name: 'feuille',
          quantity: 10
        }
      ]
    },).subscribe();
  };
}
