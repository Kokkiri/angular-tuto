import { Component, Input } from '@angular/core';
import { Cocktail } from 'src/app/shared/interface/cocktail.interface';

@Component({
  selector: 'app-cocktail-list',
  templateUrl: './cocktail-list.component.html',
  styleUrls: ['./cocktail-list.component.scss']
})
export class CocktailListComponent {
    @Input() public cocktails: Cocktail[] | null = null;
    public search = '';
}
