import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  get hobbies(): FormControl[] {
    return (this.form.get('hobbies') as FormArray).controls as FormControl[];
  }

  public form: FormGroup = new FormGroup(
    {
      name: new FormControl(''),
      email: new FormControl(''),
      hobbies: new FormArray([]),
      password: new FormControl(''),
    },
  )

  ngOnInit(): void {
    console.log((this.form.get('hobbies') as FormArray).controls as FormControl[]);
  }
  
  public addHobby = () => {
    this.hobbies.push(new FormControl(''));
  }

//   public deleteHobby(index: number) {
//     (this.form.get('hobbies') as FormArray).removeAt(index);
//   }
  
  public submit(): void {
    console.log('validator', this.form.valid);
    console.log('ma liste', this.hobbies);
  }
}
