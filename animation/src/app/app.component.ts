import { trigger, style, state, transition, animate, query, stagger } from '@angular/animations';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('square', [
      state('normal',
        style({
          backgroundColor: "white",
          border: "2px solid #444"
        })
      ),
      state('wild',
        style({
          backgroundColor: "red",
          border: "none",
          borderRadius: "50%"
        })
      ),
      transition('void => wild', animate(400)),
      transition('normal <=> wild', animate(400)),
    ]),
    trigger('list', [
      transition(
        ':enter',
        query('li', [
          style({
            opacity: 0,
            transform: 'translateX(-100px)'
          }),
          stagger(100, animate(500))
        ]
      )),
      transition(
        ':leave',
        query('li', [
          stagger(
            100,
            animate(
              500,
              style({
                opacity: 0,
                transform: 'translateX(100px)'
              })
            )
          )
        ])
      )
    ]),
  ]
})
export class AppComponent {
  public state = "wild"
  public display = true
  
  public toggle(event: any) {
    if (event.phaseName === 'done') {
      this.display = !this.display
    }
   }
}
